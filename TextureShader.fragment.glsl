#version 430 core

layout (location = 0) out vec4 color;

in vec2 tex_coord;

uniform sampler2D color_texture;

void main(void){
    color = texture(color_texture, tex_coord);
}
