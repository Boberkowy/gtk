#include "window.h"

#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cstdlib>
#include <cstring>

#include "glerror.h"

const char* kVertexShader="TextureShader.vertex.glsl";
const char* kFragmentShader="TextureShader.fragment.glsl";
const char* kColorTextureFile="earth.tga";
const char* kIceTextureFile="stars.tga";
const char* drugaplaneta = "moon.tga";

const int kPlaneM=10;
const int kPlaneN=10;
const int kTorusM=32;	
const int kTorusN=32;
const float kTorusR=2.5f;
const float kTorusr=0.5f;


Window::Window(const char * title, int width, int height){
    strcpy(title_, title);
    width_ = width;
    height_ = height;
    last_time_ = 0;
    torus_.SetInitAngle(15);
    torus_.SetVelocity(0.01f);

}

void Window::Initialize(int argc, char * argv[], int major_gl_version, int minor_gl_version){

    InitGlutOrDie(argc, argv, major_gl_version, minor_gl_version);
    InitGlewOrDie();

    std::cout << "OpenGL initialized: OpenGL version: " << glGetString(GL_VERSION) << " GLSL version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

    InitTextures();
    InitModels();

	torus_.SetTexture(color_texture_);
	torus_.SetTextureUnit(GL_TEXTURE0);
	
	torus2_.SetTexture(color_texure_moon_);
	torus2_.SetTextureUnit(GL_TEXTURE0);

    plane_.SetTexture(ice_texture_);
    plane_.SetTextureUnit(GL_TEXTURE0);



    InitPrograms();

    glUseProgram(program_);
    program_.SetTextureUnit(0);
    view_matrix_.Translate(0, 0, -10);
    program_.SetViewMatrix(view_matrix_);

    projection_matrix_ = Mat4::CreateProjectionMatrix(60, (float)width_/(float)height_, 0.1f, 100.0f);
    program_.SetProjectionMatrix(projection_matrix_);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glClearColor(0.8f, 0.9f, 1.0f, 1.0f);

}

void Window::InitGlutOrDie(int argc, char * argv[], int major_gl_version, int minor_gl_version){
    glutInit(&argc, argv);

    glutInitContextVersion(major_gl_version, minor_gl_version);
    glutInitContextProfile(GLUT_CORE_PROFILE);
#ifdef DEBUG
    glutInitContextFlags(GLUT_DEBUG);
#endif

    glutSetOption(
        GLUT_ACTION_ON_WINDOW_CLOSE,
        GLUT_ACTION_GLUTMAINLOOP_RETURNS
    );

    glutInitWindowSize(width_, height_);

    glutInitDisplayMode(GLUT_DEPTH| GLUT_DOUBLE | GLUT_RGBA);

    int window_handle = glutCreateWindow(title_);

    if( window_handle < 1) {
        std::cerr << "ERROR: Could not create a new rendering window" << std::endl;
        exit(EXIT_FAILURE);
    }


}

void Window::InitGlewOrDie(){
    GLenum glew_init_result;
    glewExperimental = GL_TRUE;
    glew_init_result = glewInit();

    if (GLEW_OK != glew_init_result) {
        std::cerr << "Glew ERROR: " << glewGetErrorString(glew_init_result) << std::endl;
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG
    if(glDebugMessageCallback){
        std::cout << "Register OpenGL debug callback " << std::endl;
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback((GLDEBUGPROC) OpenglCallbackFunction, NULL);
        GLuint unused_ids = 0;
        glDebugMessageControl(GL_DONT_CARE,
            GL_DONT_CARE,
            GL_DONT_CARE,
            0,
            &unused_ids,
            GL_FALSE);
    }
    else
        std::cout << "glDebugMessageCallback not available" << std::endl;
#endif

}

void Window::InitModels(){
	
    torus_.Initialize(kTorusN, kTorusM, kTorusR, kTorusr);
	torus2_.Initialize(100,100, 1.5f, 0.5f);
	plane_.Initialize(kPlaneM, kPlaneN);
}

void Window::InitTextures(){
	color_texture_.Initialize(kColorTextureFile);
	color_texure_moon_.Initialize(drugaplaneta);
    ice_texture_.Initialize(kIceTextureFile);
}

void Window::InitPrograms(){
    program_.Initialize(kVertexShader, kFragmentShader);
}

void Window::Resize(int new_width, int new_height){
    width_ = new_width;
    height_ = new_height;
    projection_matrix_ = Mat4::CreateProjectionMatrix(60, (float)width_/(float)height_, 0.1f, 100.0f);
    glUseProgram(program_);
    program_.SetProjectionMatrix(projection_matrix_);
    glViewport(0, 0, width_, height_);
    glutPostRedisplay();
}

void Window::KeyPressed(unsigned char key, int /*x_coord*/, int /*y_coord*/){
    switch (key){
    case 27:
      glutLeaveMainLoop();
    break;
    case ' ':
      torus_.ToggleAnimated();
	  torus2_.ToggleAnimated();
    break;
    default:
    break;
    }
}

void Window::SpecialKeyPressed(int key, int /*x_coord*/, int /*y_coord*/){
    switch (key){
    case GLUT_KEY_LEFT:
      torus_.SlowDown();
    break;
    case GLUT_KEY_RIGHT:
      torus_.SpeedUp();
    break;
      default:
      break;
    }
}


void Window::Render(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    clock_t now = clock();
    if (last_time_ == 0) last_time_ = now;
    torus_.Move((float)(now - last_time_) / CLOCKS_PER_SEC);
	torus2_.Move((float)(now - last_time_) / CLOCKS_PER_SEC);
    last_time_ = now;

	torus_.Draw(program_);
	torus2_.Draw(program_);
    plane_.Draw(program_);

    glutSwapBuffers();
    glutPostRedisplay();

}

