#ifndef PLANE_H
#define PLANE_H

#include "model.h"
#include "matma.h"
#include "program.h"

class Plane : public Model{
public:
    void Initialize(int m, int n);
    void Draw(const Program & program) const;
private:
    int n_; // mesh parameter
    int m_; // mesh parameter

};

#endif // PLANE_H
