#ifndef MODEL_H
#define MODEL_H

#include<GL/glew.h>

#include "matma.h"
#include "packages/nupengl.core.0.1.0.1/build/native/include/GL/glew.h"

typedef struct{
    float position[4];
    float color[4];
} ColorVertex;

typedef struct{
    float position[4];
    float texture[2];
} TextureVertex;


class Model{
public:
    ~Model();
    void SetTextureUnit(GLuint t){texture_unit_=t;}
    void SetTexture(GLuint t){texture_ = t;}
protected:
    Mat4 model_matrix_;
	Mat4 model_matrix_model;
    GLuint vao_;
    GLuint vertex_buffer_;
    GLuint index_buffer_;
    GLuint texture_unit_;
    GLuint texture_;
};

#endif // MODEL_H
