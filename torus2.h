#ifndef TORUS2_H
#define TORUS2_H

#include <GL/glew.h>

#include "program.h"
#include "model.h"
#include "matma.h"

class Torus2 : public Model{
public:
	Torus2(float init_velocity = 10, float init_angle = 0);
	void SetInitAngle(float angle){ angle_ = angle; }
	void SetVelocity(float velocity){ velocity_ = velocity; }
	void Initialize(int n, int m, float R, float r);
	void Draw(const Program & program) const;
	void Move(float delta_t);
	void SpeedUp();
	void SlowDown();
	void ToggleAnimated();
private:
	int n_; // mesh parameter
	int m_; // mesh parameter

	float R; //big radius
	float r; //small radius

	float angle_;
	float velocity_;
	bool animated_;

};

#endif // TORUS_H
