#ifndef WINDOW_H
#define WINDOW_H

#include <ctime>

#include "torus.h"
#include "plane.h"
#include "program.h"
#include "texture.h"
#include "matma.h"
#include "torus2.h"

class Window{
 public:
    Window(const char*, int, int);
    void Initialize(int argc, char* argv[], int major_gl_version, int minor_gl_version);
    void Resize(int new_width, int new_height);
    void Render(void);
    void KeyPressed(unsigned char key, int x_coord, int y_coord);
    void SpecialKeyPressed(int key, int x_coord, int y_coord);
 private:
    int width_;
    int height_;
    char title_[256];
    Torus torus_;
    Plane plane_;
	Texture color_texure_moon_;
    Texture color_texture_;
    Texture ice_texture_;
    Program program_;
    clock_t last_time_;
	Torus2 torus2_;

    Mat4 view_matrix_;
    Mat4 projection_matrix_;

    void InitGlutOrDie(int argc, char* argv[], int major_gl_version, int minor_gl_version);
    void InitGlewOrDie();
    void InitModels();
    void InitTextures();
    void InitPrograms();

};


#endif // WINDOW_H

